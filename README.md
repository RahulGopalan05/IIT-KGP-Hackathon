# IIT-KGP Data Science Hackathon

Welcome to our submission for the Data Science Hackathon conducted by IIT-Kharagpur in January 2024. The objective of this hackathon is to formulate an algorithm that delivers returns on BTC-USDT using historical data from 2018 to 2022.

## Repository Contents

### Notebooks

1. **GoldenCross.ipynb**: Implementation of a trading strategy based on the Golden Cross (SMA crossover).

2. **IITKGP-KWANT.ipynb**: Notebook related to KWANT, possibly for quantum computing or optimization.

3. **UnsupervisedModel.ipynb**: Implementation of an unsupervised learning model. (Needs further exploration)

4. **whit_model.ipynb**: Final XGBoost model for BTC-USDT returns. ( the main final model)

### Python Scripts

1. **blek_goldcross_final.py**: Python script related to "blek_goldcross_final". (Need to explore the content and provide a brief description).

2. **blek_unsupervisedmode_final.py**: Python script related to "blek_unsupervisedmode_final". (Need to explore the content and provide a brief description).

3. **bollinger_bands.py**: Python script related to Bollinger Bands.

4. **updated_model, updated_model2, updated_model3, updated_model4, updated_model5**: Folders with iterations and improvements on the XGBoost model for BTC-USDT returns.

### Data

1. **BTC-USD.csv**: Dataset containing historical data for BTC-USD.

2. **sp500_historical_data.csv**: Dataset containing historical data for the S&P 500.

### Images

1. **BTCUSDT-2018-01-01.png, BTCUSDT-2022-31-01.png**: Images related to BTCUSDT. (Check for details)

## Usage

1. **Golden Cross Strategy**: Check the implementation in GoldenCross.ipynb for a trading strategy based on SMA crossover.

2. **Unsupervised Model**: Explore UnsupervisedModel.ipynb for the implementation and results of an unsupervised learning model.

3. **XGBoost Model**: Find the final XGBoost model in whit_model.ipynb, and explore the updated_model folders for iterations and improvements.

## Acknowledgments

Special thanks to IIT-Kharagpur for organizing this hackathon. 

Feel free to explore, experiment, and contribute to enhance the performance of our models.
